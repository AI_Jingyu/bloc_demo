import 'dart:async';

/*
single subscription
*/
void main(){
  
  //initiation of single subscription
  final StreamController ctrl=StreamController();

  //stream subscription 
  final StreamSubscription subscription=ctrl.stream.listen((data)=>print('$data'));

  //add data to stream
  ctrl.sink.add('test example');
  ctrl.sink.add(666666);
  ctrl.sink.add({'a':'test case 1', 'b':'test case 2'});
  ctrl.sink.add(66.666);

  ctrl.close();
}